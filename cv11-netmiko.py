#!/usr/bin/env python3

from netmiko import ConnectHandler

# cisco = {
#     "device_type": "cisco_ios",
#     "host": "158.193.152.71",
#     "username": "admin",
#     "password": "class",
#     "port": 22
# }

# pripojenie = ConnectHandler(**cisco)

# vystup = pripojenie.send_command("show version")
# print(vystup)

# vystup = pripojenie.send_command("show ip int brief")
# print(vystup)

# loop = [
#     "int lo100",
#     "ip add 1.2.3.4 255.255.255.255",
#     "no shut"         
# ]
# vystup = pripojenie.send_config_set(loop)
# print(vystup)

# vystup = pripojenie.send_command("show ip int brief")
# pole = vystup.strip("\n").split(" ")
# for i in pole:
#     if i == "":
#         pole.remove(i)
# print(pole)


mikrotik = {
    "device_type": "mikrotik_routeros",
    "host": "158.193.152.116",
    "username": "admin",
    "password": "class",
    "port": 22
}

pripojenie = ConnectHandler(**mikrotik)

vystup = pripojenie.send_command("int print terse")
print(vystup)

vystup = pripojenie.send_command("int bridge add name=lo100")
print(vystup)

vystup = pripojenie.send_command("int print")
print(vystup)