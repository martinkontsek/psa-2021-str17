#!/usr/bin/env python3
from tkinter.constants import DISABLED, NORMAL
import urllib.request
import xml.etree.ElementTree as XML_ET

import tkinter as tk

def nastavOkno(root):
    return

def parsujRSS(paURL):
    otvorene_rss = urllib.request.urlopen(paURL)
    stranka = otvorene_rss.read()
    strom = XML_ET.fromstring(stranka)
    
    returntext = ""

    for channel in strom:
        for item in channel:
            title = ""
            desc = ""
            link = ""
            pubDate = ""
            for prvok in item:
                if prvok.tag == "title":
                    title = prvok.text
                elif prvok.tag == "description":
                    desc = prvok.text
                elif prvok.tag == "link":
                    link = prvok.text
                elif prvok.tag == "pubDate":
                    pubDate = prvok.text
            returntext += "Nadpis: {} - {}: Link: {}, {} \n\n".format(
                title, desc, link, pubDate)

    return returntext

def vykonajTlacidlo(textPole, urlPole):
    textPole.config(state=NORMAL)
    textPole.delete(1.0, tk.END)
    textPole.insert(tk.END, parsujRSS(urlPole.get()))
    textPole.config(state=DISABLED)
    
if __name__ == "__main__":
    # vystup = parsujRSS()
    # print(vystup)

    root = tk.Tk()
    nastavOkno(root)
    root.title("RSS Parser v0.1")
    root.geometry("665x480")
    root.resizable(False, False)

    urlPole = tk.Entry(root)
    urlPole.grid(row=1, column=0, padx=10, sticky="we", ipadx=190)
    urlPole.insert(0, "http://www.dsl.sk/export/rss_articles.php")
    
    urlPopis = tk.Label(root, text="URL")
    urlPopis.grid(row=0, column=0, padx=10, pady=2, sticky="w")

    parsujTlacidlo = tk.Button(root, text="Parsuj", command=lambda:
        vykonajTlacidlo(textPole, urlPole) )
    parsujTlacidlo.grid(row=1, column=1, sticky="e", padx=10)
    

    textPole = tk.Text(root)
    textPole.grid(row=2, column=0, padx=10, columnspan=2)

    root.mainloop()