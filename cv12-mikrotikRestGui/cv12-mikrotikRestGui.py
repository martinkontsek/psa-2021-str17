#!/usr/bin/env python3

import requests
from flask import Flask, request, render_template

IP="158.193.152.116"
PORT=443
USER="admin"
PASS="class"

def dajIP(paIP, paPort, paUser, paPass):
    odpoved = requests.get("https://{}:{}/rest/ip/address".format(paIP, paPort),
                        auth=(paUser,paPass),
                        verify=False)

    if odpoved.status_code == 200:
          return odpoved.json()

    else:
        print(odpoved.status_code)
        print(odpoved.json())
        return None

def dajRozhrania(paIP, paPort, paUser, paPass):
    odpoved = requests.get("https://{}:{}/rest/interface".format(paIP, paPort),
                        auth=(paUser,paPass),
                        verify=False)

    if odpoved.status_code == 200:
        vystup = list()
        for polozka in odpoved.json():
            slovnik = dict(name=polozka["name"])
            vystup.append(slovnik)
        return vystup
    else:
        print(odpoved.status_code)
        print(odpoved.json())
        return None

def nastavIP(paIP, paPort, paUser, paPass, paAdresa, paRozhranie):
    konfig = {"address": paAdresa, "interface": paRozhranie}

    odpoved = requests.put("https://{}:{}/rest/ip/address".format(paIP, paPort),
                        auth=(paUser,paPass),
                        verify=False,
                        json=konfig)

    if odpoved.status_code == 201:
          return odpoved.json()

    else:
        print(odpoved.status_code)
        print(odpoved.json())
        return None

def vytvorLoop(paIP, paPort, paUser, paPass, paRozhranie):
    konfig = {"name": paRozhranie}

    odpoved = requests.put("https://{}:{}/rest/interface/bridge".format(paIP, paPort),
                        auth=(paUser,paPass),
                        verify=False,
                        json=konfig)

    if odpoved.status_code == 201:
          return odpoved.json()

    else:
        print(odpoved.status_code)
        print(odpoved.json())
        return None

def dajRozhraniaIP():
    rozhrania = dajRozhrania(IP, PORT, USER, PASS)
    ip = dajIP(IP, PORT, USER, PASS)

    vystup = list()
    for rozhranie in rozhrania:
        adresa = ""
        for i in ip:
            if i["interface"] == rozhranie["name"]:
                adresa += i["address"] + " "
        vystup.append(dict(name=rozhranie["name"], address=adresa))
    return vystup

app = Flask(__name__)

@app.route("/")
def index():
    rozhrania = dajRozhraniaIP()
    # vystup = "<html><header>"
    # vystup += '<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">'
    # vystup += '<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>'
    # vystup += "</header>"
    # vystup += '<body><table class="table table-striped table-hover"> <tr><td>Nazov</td><td>IP</td></tr>'
    # for i in rozhrania:
    #     vystup += "<tr><td>{}</td><td>{}</td></tr>".format(i["name"], i["address"])
    # vystup += "</table>"

    # vystup += "<a href='/ip'>Pridaj IP</a><br />"
    # vystup += "<a href='/loop'>Pridaj Loopback</a><br />"
    # vystup += "</body></html>"
    # return vystup
    return render_template("index.html", data=rozhrania)

@app.route("/ip", methods=("GET", "POST"))
def pridajIP():
    if request.method == "GET":
        return render_template("cv12-pridajIP.html")
    else:
        ip = request.form["ip"]
        rozhranie = request.form["int"]
        if nastavIP(IP, PORT, USER, PASS, ip, rozhranie) != None:
            return render_template("vysledok.html", nadpis="Pridaj IP", sprava="Pridanie uspesne.")
        else:
            return render_template("vysledok.html", nadpis="Pridaj IP", sprava="Chyba!!!")

@app.route("/loop", methods=("GET", "POST"))
def pridajLoop():
    if request.method == "GET":
        return render_template("cv12-pridajLoop.html")
    else:
        rozhranie = request.form["int"]
        if vytvorLoop(IP, PORT, USER, PASS, rozhranie) != None:
            return render_template("vysledok.html", nadpis="Vytvor Loop", sprava="Vytvorenie uspesne.")
        else:
            return render_template("vysledok.html", nadpis="Vytvor Loop", sprava="Chyba!!!")


if __name__ == "__main__":
    # vystup = nastavIP(IP, PORT, USER, PASS, "192.168.0.1/32", "lo0")
    # print(vystup)
    # vystup = vytvorLoop(IP, PORT, USER, PASS, "lo1")
    # print(vystup)
    # vystup = dajRozhrania(IP, PORT, USER, PASS)
    # print(vystup)
    app.run("0.0.0.0", 8888)