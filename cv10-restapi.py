#!/usr/bin/env python3

from flask import Flask, Response, jsonify, request
import json

KNIHY = [
    {"id": 0, "nazov": "Harry Potter 1", "popis": "Harry Potter a Kamen Mudrcov"},
    {"id": 1, "nazov": "Harry Potter 2", "popis": "Harry Potter a Tajomna Komnata"},
    {"id": 2, "nazov": "Harry Potter 3", "popis": "Harry Potter a Vazen z Azkabanu"},
    {"id": 3, "nazov": "Python for Dummies", "popis": "Great book about Python foundation"},
    {"id": 4, "nazov": "Python v sietovych aplikaciach", "popis": "Tajna verzia skript na predmet"},
]

app = Flask(__name__)

@app.route("/")
def index():
    return "<h1>Index stranky vo Flask</h1>"

@app.route("/knihy")
def dajKnihy():
    # vystup = ""
    # for kniha in KNIHY:
    #     vystup += "Nazov: {}, popis: {}\n".format(kniha["nazov"], kniha["popis"])
    # return vystup, 200
    resp = Response(response=json.dumps(KNIHY), status=200, mimetype="application/json")
    return resp

@app.route("/knihy/<int:id>")
def dajKnihu(id):
    for kniha in KNIHY:
        if kniha["id"] == id:
            # return "Nazov: {}, popis: {}".format(kniha["nazov"], kniha["popis"])
            return jsonify(kniha)
    # return "Zadany index neexistuje.", 404
    return jsonify({"error": "Zadany index neexistuje."}), 404

@app.route("/knihy", methods=["POST"])
def pridajKnihu():
    lastID = 0
    for kniha in KNIHY:
        if kniha["id"] > lastID:
            lastID = kniha["id"]

    novaKniha = request.json
    novaKniha["id"] = lastID+1

    KNIHY.append(novaKniha)

    return jsonify(novaKniha), 201

@app.route("/knihy/<int:id>", methods=["DELETE"])
def vymazKnihu(id):
    for kniha in KNIHY:
        if kniha["id"] == id:
            KNIHY.remove(kniha)
            return jsonify(kniha)
    return jsonify({"error": "Zadany index neexistuje."}), 404

if __name__ == "__main__":
    app.run("0.0.0.0", 8888)