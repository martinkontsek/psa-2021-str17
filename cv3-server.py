#!/usr/bin/env python3

import socket
from enum import IntEnum

ADRESA = "0.0.0.0"
PORT = 9999

class Sprava:
    def __init__(self, paOd, paKomu, paOperacia, paText):
        self.aOd = paOd
        self.aKomu = paKomu
        self.aOperacia = paOperacia
        self.aText = paText

class Operacia(IntEnum):
    LOGIN = 1
    EXIT = 2
    USERS = 3

def ObsluzKlienta(paClientSock, paClientAddr):
    while(True):
        data = paClientSock.recv(1500)
        print("Pripojil sa klient IP {}:{}".format(paClientAddr[0], paClientAddr[1]))
        print(data.decode())


if __name__ == "__main__":
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((ADRESA, PORT))
    sock.listen(10)

    while(True):
        (clientSock, clientAddr) = sock.accept()
        ObsluzKlienta(clientSock, clientAddr)
