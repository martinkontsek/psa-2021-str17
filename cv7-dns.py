#!/usr/bin/env python3

import struct
import socket

DNS_SERVER = "1.1.1.1"
DNS_PORT = 53

transactionid = 0x1234
flags = 0x0100 # DNS ziadost
question_count = 0x0001
answer_count = 0
authority_count = 0
additional_count = 0

dns_header = struct.pack("!6H",
                        transactionid,
                        flags,
                        question_count,
                        answer_count,
                        authority_count,
                        additional_count)

question_text = input("Zadajte DNS meno: ") #uniza.sk
qtype = 1 # 1 - A, 2-NS, 5-CNAME, 6-SOA
qclass = 1

question_list = question_text.split(".")
question = bytes()
for label in question_list:
    question += struct.pack("!B",len(label))
    question += label.encode()
question += struct.pack("!B", 0)
end_of_query = struct.pack("!2H", qtype, qclass)

dns_query = dns_header + question + end_of_query


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(("0.0.0.0", 50000))

sock.sendto(dns_query, (DNS_SERVER, DNS_PORT))

response, addr = sock.recvfrom(1000)
resp_type = response[3:4]
resp_address = response[-4:]

print("Odpoved od {}:{} - Type: {}, adresa: {}".format(addr[0],
                                                addr[1],
                                                int.from_bytes(resp_type, byteorder="big"),
                                                socket.inet_ntoa(resp_address)))

sock.close()