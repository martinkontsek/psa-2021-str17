#!/usr/bin/env python3

import socket
from enum import IntEnum
import json

ADRESA = "127.0.0.1"
PORT = 9999

class Sprava:
    def __init__(self, paOd, paKomu, paOperacia, paText):
        self.aOd = paOd
        self.aKomu = paKomu
        self.aOperacia = paOperacia
        self.aText = paText
    @staticmethod
    def json_decoder(obj):
        return Sprava(obj['aOd'], obj['aKomu'], obj['aOperacia'], obj['aText'])

class Operacia(IntEnum):
    LOGIN = 1
    EXIT = 2
    USERS = 3

def napoveda():
    print("***Napoveda")
    print("  \q Ukonci program")
    print("  \l Vypis prihlasenych")
    print("  \h Vypis napovedu")

if __name__ == "__main__":
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ADRESA, PORT))

    print("Vita vas CHAT klient.")

    nick = input("Zadajte svoj nick: ")
    napoveda()

    sprava = Sprava(nick, '', Operacia.LOGIN, '')
    jsonStr = json.dumps(sprava.__dict__)
    sock.send(jsonStr.encode())

    bezim = True
    while(bezim):
        prikaz = input("Zadajte prikaz: ")

        if prikaz[0] == "\\":
            if prikaz[1] == "h":
                napoveda()
                continue
            if prikaz[1] == "q":
                sprava = Sprava(nick, '', Operacia.EXIT, '')
                # poslanie spravy
                jsonStr = json.dumps(sprava.__dict__)
                sock.send(jsonStr.encode())
                # uzavri spojenie
                sock.close()
                # ukoncim uspesne program
                exit(0)
            if prikaz[1] == "l":
                sprava = Sprava(nick, '', Operacia.USERS, '')
                # poslanie spravy
                jsonStr = json.dumps(sprava.__dict__)
                sock.send(jsonStr.encode())
                # prijmem zoznam userov
                data = sock.recv(1500)
                sprava = json.loads(data.decode(), object_hook=Sprava.json_decoder)
                print("Zoznam prihlasenych: " + sprava.aText)

        # Posielam textovu spravu vo formate user:sprava
        vystupPrikazu = prikaz.split(":")
        sprava = Sprava(nick, vystupPrikazu[0], '', vystupPrikazu[1])
        # poslanie spravy
        jsonStr = json.dumps(sprava.__dict__)
        sock.send(jsonStr.encode())