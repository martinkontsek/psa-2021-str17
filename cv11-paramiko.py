#!/usr/bin/env python3

from paramiko import SSHClient, AutoAddPolicy

client = SSHClient()
client.load_system_host_keys()
client.set_missing_host_key_policy(AutoAddPolicy())
client.connect("158.193.152.116", port=22, username="admin", password="class", timeout=60, look_for_keys=False, allow_agent=False)

# stdin, stdout, stderr = client.exec_command("show version")
# for riadok in stdout:
#     print(riadok.strip("\n"))

# stdin, stdout, stderr = client.exec_command("show ip int brief")
# for riadok in stdout:
#     pole = riadok.strip("\n").split(" ")
#     for i in pole:
#         if i == "":
#             pole.remove(i)
#     print(pole)

# stdin, stdout, stderr = client.exec_command("conf t\nint lo0\nip add 1.1.1.1 255.255.255.255")
# for riadok in stdout:
#     print(riadok.strip("\n"))

# stdin, stdout, stderr = client.exec_command("ip address print")
# for riadok in stdout:
#     print(riadok.strip("\n"))

# stdin, stdout, stderr = client.exec_command("int pr")
# for riadok in stdout:
#     print(riadok.strip("\n"))

# stdin, stdout, stderr = client.exec_command("int bridge add name=lo0 disabled=no")
# for riadok in stdout:
#     print(riadok.strip("\n"))

stdin, stdout, stderr = client.exec_command("int pr terse")
for riadok in stdout:
    print(riadok.strip("\n").split(" "))

client.close()