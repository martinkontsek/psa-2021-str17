#!/usr/bin/env python3
APP_VERSION = "1.0"

class Movie:
    def __init__(self, paTitle, paYear, paRating, paDuration, paGenre):
        self.aTitle = paTitle
        self.aYear = paYear
        self.aRating = paRating
        self.aDuration = paDuration
        self.aGenre = paGenre
    def toString(self):
        return "{:20} {:10} {} {:5}% {:7}m".format(self.aTitle,
                                                    self.aGenre,
                                                    self.aYear,
                                                    self.aRating,
                                                    self.aDuration)

class MovieLibrary:
    def __init__(self):
        self.aEntries = list()
    def addEntry(self, paMovie):
        self.aEntries.append(paMovie)
    def printLibrary(self, paPrintIndex):
        if paPrintIndex:
            indexString = " "*4
        else:
            indexString = ""
        print("{}{:20} {:10} {} {:5} {:7}".format(indexString, "Title", "Genre", "Year", "Rating", "Duration"))
        print(indexString + "-"*52)
        index = 0
        for movie in self.aEntries:
            if paPrintIndex:
                print("{:3} ".format(index) + movie.toString())
            else:                
                print(movie.toString())
            index += 1
    def removeEntry(self, paIndex):
        self.aEntries.pop(paIndex)

def addMovie(paMovieLibrary):
    title = input("Enter movie title: ")
    genre = input("Enter movie genre: ")
    year = input("Enter year: ")
    rating = input("Enter rating in %:")
    duration = input("Enter duration in minutes: ")

    movie = Movie(title,year,rating,duration,genre)
    paMovieLibrary.addEntry(movie)

def removeMovie(paMovieLibrary):
    paMovieLibrary.printLibrary(True)

    id = input("Enter move ID for removal: ")
    paMovieLibrary.removeEntry(int(id))

def menu(paMovieLibrary):
    while True:
        print("Welcome to Movie Library v{}\n please select an option: ".format(APP_VERSION))
        print("Add Movie (1)")
        print("Remove Movie (2)")
        print("Show library content (3)")
        print("Exit program (q)")
                
        choice = input("Your selection: ")

        if choice == "1":
            addMovie(paMovieLibrary)
        elif choice == "2":
            removeMovie(paMovieLibrary)
        elif choice == "3":
            paMovieLibrary.printLibrary(False)
        elif choice == "q":
            print("Bye!!!")
            exit(0)
        else:
            print("Incorrect choice!!!")





if __name__ == "__main__":
    data = MovieLibrary()
    menu(data)